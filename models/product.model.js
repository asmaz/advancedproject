const mongoose = require('mongoose');
const { schema } = require('./order.model');
const Schema = mongoose.Schema;

 const ProductSchema = new Schema({
    reference: {
        type: String,
        trim: true,
        required: [true,"Reference is required"]
    },
    description: {
        type: String,
        trim: true,
        required: [true,"Description is required"]
    },
    price: {
        type: String,
        trim: true,
        required: [true,"Price is required"]
    },
    subcategory:{
        type:Schema.Types.ObjectId,
        ref:'SubCategory'
    }
  
})

 module.exports = mongoose.model('Product', ProductSchema);
