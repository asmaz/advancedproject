const mongoose = require('mongoose');
const Schema = mongoose.Schema;

 const CategorySchema = new Schema({
    name: {
        type: String, 
        trim: true,
        required: [true,"Name is required"]
    },
    description: {
        type: String,
        trim: true,
        required: [true,"Description is required"]
    },
   
  
})

 module.exports = mongoose.model('Category', CategorySchema);
