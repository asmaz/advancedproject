const mongoose = require('mongoose');
const Schema = mongoose.Schema;

 const GalorySchema = new Schema({
    name: {
        type: String, 
        trim: true,
        
    },
   
   product:{
        type:Schema.Types.ObjectId,
        ref:'Product'
    }
  
})

 module.exports = mongoose.model('galory',GalorySchema);
