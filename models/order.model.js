const mongoose = require('mongoose');
const Schema = mongoose.Schema;

 const OrderSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true,"Name is required"]
    },
    description: {
        type: String,
        trim: true,
        required: [true,"Description is required"]
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    product:[{
        type:Schema.Types.ObjectId,
        ref:'Product'
    }]

  
})

 module.exports = mongoose.model('Order', OrderSchema);
