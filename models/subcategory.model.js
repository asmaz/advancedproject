
const mongoose=require('mongoose');
const Schema = mongoose.Schema;

const SubCategorySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true,"Name is required"]

    },
    description: {
        type: String,
        trim: true,
        required: [true,"Description is required"]
    },
    category:{
        type:Schema.Types.ObjectId,
        ref:'Category'
    }
  
})

 module.exports = mongoose.model('SubCategory', SubCategorySchema);
