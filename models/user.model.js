const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const saltRounds = 10;

// Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, "Name is required"],
  },
  email: { 
    type: String,
    required: [false, "Email is required"],
    // sync validation
    validate: {
      validator: function (v) {
        // regex product code must have XXXX-XXXX-XXXX format
        // return true to pass the validation
        // return false to fail the validation
        return (/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v));
    },
      // message to return if validation fails
      message: (props) => `${props.value} is not a valid code format!`,
    },
    required: [true, "email required"],
  },
  phone: {
    type: String,
    required: [false, "Phone is required"],
    // sync validation
    validate: {
      validator: function (v) {
        // regex product code must have XXXX-XXXX-XXXX format
        // return true to pass the validation
        // return false to fail the validation
        return /\d{2}-\d{3}-\d{3}/.test(v);
      },
      // message to return if validation fails
      message: (props) => `${props.value} is not a valid code format!`,
    },
    
  },
  password: {
    type: String,
    trim: true,
    required: [false, "Password is required"],
  },
  state: {
    type: String,
    trim: true,
    default:"0"
   },
   photo:[{
    type: String,
    trim: true,
    required:true,
   }],
 
});
// hash user password before saving into database
UserSchema.pre("save", function (next) {
  this.password = bcrypt.hashSync(this.password, saltRounds);
  next();
});
module.exports = mongoose.model("User", UserSchema);
