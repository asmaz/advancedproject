const express = require('express')
const cors =require('cors')
const  swaggerUi = require('swagger-ui-express')
swaggerDocument =require('./config/Swagger.json')
const bodyParser = require('body-parser')
 const db = require('./config/database')
const userRouter = require('./routers/user.router')
const oredrRouter = require('./routers/order.router')
const categoryRouter = require('./routers/category.router')
const subcategoryRouter = require('./routers/subcategory.router')
const productRouter = require('./routers/product.router')
const galoryRouter = require('./routers/galory_router')


var app = express()
app.set('secretKey','10')
app.use(cors())


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


app.use('/api/users', userRouter)
app.use('/api/orders',oredrRouter)
app.use('/api/category',categoryRouter)
app.use('/api/subcategory',subcategoryRouter)
app.use('/api/product',productRouter)
app.use('/api/galory',galoryRouter)



app.get("/",function(req,res){
    res.send('helloo world!')
})

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error('Not Found');
       err.status = 404;
       next(err);
   });
   // handle errors
   app.use(function(err, req, res, next) {
    console.log(err);
    
     if(err.status === 404)
      res.status(404).json({message: "Not found"});
     else 
       res.status(500).json({message: "Something looks wrong :( !!!"});

       
   });
app.listen(3000,function(){

    console.log('running with 3000')
})
