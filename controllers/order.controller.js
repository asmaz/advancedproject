const OrderModel = require('../models/order.model')

module.exports = {
    create: async function (req, res) {
        console.log("okkkk create ", req.body)

        try {

            const Order = await OrderModel.create(req.body)
  
            res.status(200).json({msg: 'Order added', status: 200, data: Order})

        } catch (err) {
            res.status(400).json({msg: err['message'], status: 400, data: null})
        }
    },

    
    getAll: async function (req, res) {
        
        
        try {
             
            const Orders = await OrderModel.find().select("name description user product").
            limit(parseInt(req.params['limit'])
            ).skip(parseInt(req.params['page'])*parseInt(req.params['limit']))
            .sort({name:"asc"}).populate("user","name").populate("product","reference")
            res.status(200).json({msg: ' All Order ', status: 200, data: Orders, size:Orders.length})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateOrder: async function (req, res) {
      
        try {
            const Order = await OrderModel.updateOne({
                _id: req.params.id
            }, req.body)
            res.status(200).json({msg: '  Order Updated', status: 200, data: Order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteOrder: async function (req, res) {
       
        try {

            const Order = await OrderModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  Order Deleted ', status: 200, data: Order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getOrderById: async function (req, res) {
        try {

            const Order = await OrderModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get Order ', status: 200, data: Order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    

}
