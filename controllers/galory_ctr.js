const galoryModel = require('../models/galory_model')

module.exports = {


    create: async function (req, res) {

        console.log("okkkk create ", req.files)
        
        try {
            await req.files.array.forEach(element => {
                console.log("element",element.filename)
                const gallery = galoryModel.create({name:element.filename  , product:req.body.product})
                
            });
    

            res.status(200).json({msg: 'galory added', status: 200, data: galory})


        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },

    getAll: async function (req, res) {
        
        
        try {

            const galory = await galoryModel.find().select("name").limit(parseInt(req.params['limit'])).skip(parseInt(req.params['page'])*parseInt(req.params['limit'])).sort({name:"asc"})
            res.status(200).json({msg: ' All galory ', status: 200, data: galory, size: galory.length})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    update: async function (req, res) {
      
        try {
            const galory = await galoryModel.updateOne({
                _id: req.params.id
            }, req.body)
            res.status(200).json({msg: '  Updated', status: 200, data: galory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    delete: async function (req, res) {
       
        try {

            const galory = await galoryModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: ' Deleted ', status: 200, data: galory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getgaloryById: async function (req, res) {
        try {

            const galory = await galoryModel.findOne({_id: req.params.id})
            res.status(200).json({msg: 'get galory', status: 200, data: galory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    getgalleryByIdProd: async function (req, res) {
        console.log('prod :', req.params.id)
        try {
            const gallery = await galleryModel.find({ product: req.params.id })
            res.status(200).json({ msg: 'gallery geted', status: 200, data: gallery });
        }
        catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })
        }
    },

    

}
