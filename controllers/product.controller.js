const ProductModel = require('../models/product.model')

module.exports = {


    create: async function (req, res) {
        console.log("okkkk create ", req.body)

        try {

            const Product = await ProductModel.create(req.body)
            res.status(200).json({msg: 'Product added', status: 200, data: Product})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },

    getAll: async function (req, res) {
        
        
        try {

            const Product = await ProductModel.find().select("reference description price subcategory").limit(parseInt(req.params['limit'])).skip(parseInt(req.params['page'])*parseInt(req.params['limit'])).sort({reference:"asc"}).populate("subcategory")
            res.status(200).json({msg: ' All Product ', status: 200, data: Product,size: Product.length})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateproduct: async function (req, res) {
      
        try {
            const Product = await ProductModel.updateOne({
                _id: req.params.id
            },
            {$set:req.body},req.body
            )
            res.status(200).json({msg: ' Product Updated', status: 200, data: Product})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteproduct: async function (req, res) {
       
        try {

            const Product = await productModel.findByIdAndDelete ({_id: req.params.id})
            res.status(200).json({msg: '  Product Deleted ', status: 200, data: Product})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})
            console.log (err)

        }

    },
    getProductById: async function (req, res) {
        try {

            const Product = await productModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get Product ', status: 200, data: Product})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    

}
