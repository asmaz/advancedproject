const SubCategoryModel = require('../models/subcategory.model')

module.exports = {

    create: async function (req, res) {
        console.log("okkkk create ", req.body)

        try {

            const subCategory = await SubCategoryModel.create(req.body)
            res.status(200).json({msg: 'subCategory added', status: 200, data: subCategory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },

    getAll: async function (req, res) {
        
        
        try {

            const subCategory = await SubCategoryModel.find().select("name description category").limit(parseInt(req.params['limit'])).skip(parseInt(req.params['page'])*parseInt(req.params['limit'])).sort({name:"asc"}).populate("category")
            res.status(200).json({msg: ' All subCategory ', status: 200, data: subCategory ,size:subCategory.length})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateSubCategory: async function (req, res) {
      
        try {
            const subCategory = await SubCategoryModel.updateOne({
                _id: req.params.id
            }, req.body)
            res.status(200).json({msg: '  subCategory Updated', status: 200, data: subCategory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteSubCategory: async function (req, res) {
       
        try {

            const subCategory = await SubCategoryModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  subCategory Deleted ', status: 200, data: subCategory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getsubcategoryById: async function (req, res) {
        try {

            const subcategory = await SubCategoryModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get subcategory ', status: 200, data: subcategory})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    

}
