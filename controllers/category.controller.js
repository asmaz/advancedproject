const CategoryModel = require('../models/category.model')

module.exports = {


    create: async function (req, res) {
        console.log("okkkk create ", req.body)

        try {

            const Category = await CategoryModel.create(req.body)
            res.status(200).json({msg: 'Category added', status: 200, data: Category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },

    getAll: async function (req, res) {
        
        
        try {

            const Category = await CategoryModel.find().select("name description").limit(parseInt(req.params['limit'])).skip(parseInt(req.params['page'])*parseInt(req.params['limit'])).sort({name:"asc"})
            res.status(200).json({msg: ' All Category ', status: 200, data: Category, size: Category.length})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateCategory: async function (req, res) {
      
        try {
            const Category = await CategoryModel.updateOne({
                _id: req.params.id
            }, req.body)
            res.status(200).json({msg: '  Category Updated', status: 200, data: Category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteCategory: async function (req, res) {
       
        try {

            const Category = await CategoryModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  Category Deleted ', status: 200, data: Category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getCategoryById: async function (req, res) {
        try {

            const Category = await CategoryModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get Category ', status: 200, data: Category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    

}
