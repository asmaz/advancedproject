const userModel = require("../models/user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const fs=require("fs")
const nodemailer =require("nodemailer")
var randtoken = require("rand-token");
var refreshTokens = {};

module.exports = {
  create: async function (req, res) {
        console.log("body of create user",req.body)
        try {
            const User = await userModel.create({name:req.body.name,email:req.body.email,
                password:req.body.password,photo:req.file.filename});
            res.status(200).json({ msg: 'user added ', status: 200, data: User });

        }
        catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })
        }

    },

  getAll: async function (req, res) {
    try {
      const Users = await userModel.find().select("name email phone password");
      res.status(200).json({ msg: " All user ", status: 200, data: Users });
    } catch (err) {
      res.status(400).json({ msg: err, status: 400, data: null });
    }
  },
  updateUser: async function (req, res) {
    try {
      const user = await userModel
        .findByIdAndUpdate(
          {
            _id: req.params.id,   
          },
          req.body
        )
        .select("name email");
      res.status(200).json({ msg: "  user Updated", status: 200, data: user });
    } catch (err) {
      res.status(400).json({ msg: err, status: 400, data: null });
    }
  },

  deleteUser: async function (req, res) {
    try {
      const user = await userModel.findByIdAndDelete({ _id: req.params.id });
      fs.unlinkSync('./uploads/'+user.photo)
      res.status(200).json({ msg: "  user Deleted ", status: 200, data: user });
    } catch (err) {
      res.status(400).json({ msg: err, status: 400, data: null });
    }
  },
  getUserById: async function (req, res) {
    try {
      const user = await userModel.findOne({ _id: req.params.id });
      res.status(200).json({ msg: "  get user ", status: 200, data: user });
    } catch (err) {
      res.status(400).json({ msg: err, status: 400, data: null });
    }
  },

  authenticate: function (req, res, next) {
    userModel.findOne({ email: req.body.email }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (bcrypt.compareSync(req.body.password, userInfo.password)) {
          var refreshToken = randtoken.uid(256);
          refreshTokens[refreshToken] = userInfo._id;

          const token = jwt.sign(
            { id: userInfo._id },
            req.app.get("secretKey"),
            { expiresIn: "1h" }
          );
          res.json({
            status: "success",
            message: "user found!!!",
            data: {
              user: userInfo,
              accesstoken: token,
              refreshToken: refreshToken,
            },
          });
        } else {
          res.json({
            status: "error",
            message: "Invalid email/password!!!",
            data: null,
          });
        }
      }
    });
  },

  refreshToken: function (req, res, next) {
    //app.post('/token', function (req, res, next) {
    var id = req.body._id;
    var refreshToken = req.body.refreshToken;
    if (refreshToken in refreshTokens && refreshTokens[refreshToken] == id) {
      var user = {
        id: id,
      };
      var token = jwt.sign(user, req.app.get("secretKey"), { expiresIn: 3600 });
      res.json({ accesstoken: token });
    } else {
      res.send(401);
    }
  },
  logout: function (req, res, next) {
    var refreshToken = req.body.refreshToken;
    if (refreshToken in refreshTokens) {
      delete refreshTokens[refreshToken];
    }
    res.status(204).json({ msg: 'Token expiré' ,status:204});
  },
  Send_mailer: function (req, res, next) {
    var transporter = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      ssl: false,
      tls: true,
      auth: {
        user: '911d265474fc37',
        pass: '38e7b1143b24f4'
      }
    });
    var mailOptions = {
      from: req.body.from,
      to: req.body.to,
      subject: req.body.subject,
    html: req.body.html,
    attachments: req.body.attachments
   
 
    };
    
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        res.json(info)
      }
    });
  },
 verifyUser :function(req,res,next){
     try{
  userModel.findByIdAndUpdate({_id:req.params.id},req.body,{new:true})
  res.json(user)}
  catch(err){
    res.json(err)
  }
 }
};
