const orderController = require('../controllers/order.controller')
const express = require('express')
 const router = express.Router()

router.post('/',orderController.create)
router.put('/:id',orderController.updateOrder)
router.delete('/:id',orderController.deleteOrder)
router.get('/:page/:limit',orderController.getAll)
router.get('/:id',orderController.getOrderById)

module.exports=router
