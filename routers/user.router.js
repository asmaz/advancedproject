const userController = require("../controllers/user.controller");
const upload =require('../middelware/upload')
const express = require("express");
const router = express.Router();
const jwt =require("jsonwebtoken")
// public router
router.post("/",upload.single('photo'), userController.create);
router.post("/auth", userController.authenticate);
//private router
router.put("/:id",upload.array('image'), userController.updateUser);
router.delete("/:id", userController.deleteUser);
router.get("/", userController.getAll);
router.get("/:id", userController.getUserById);
router.post("/refresh", validateUser, userController.refreshToken);
router.delete("/logout",validateUser, userController.logout);
router.post("/mail", userController.Send_mailer);
router.post("/validate", userController.verifyUser);

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
      if (err) {
        res.json({status:"error", message: err.message, data:null});
      }else{
        // add user id to request
        req.body.userId = decoded.id;
        next();
      }
    })}
      
module.exports = router;
