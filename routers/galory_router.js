const galoryController = require('../controllers/galory_ctr')
const express = require('express')
const router = express.Router()
const upload =require('../middelware/upload')

router.post('/',upload.array('images',10),galoryController.create)
router.put('/:id',galoryController.update)
router.delete('/:id',galoryController.delete)
router.get('/:page/:limit',galoryController.getAll)
router.get('/:id',galoryController.getgaloryById)
router.get('/prod/:id',galoryController.getgalleryByIdProd)

module.exports=router
 