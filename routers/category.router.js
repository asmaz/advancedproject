const categoryController = require('../controllers/category.controller')
const express = require('express')
 const router = express.Router()

router.post('/',categoryController.create)
router.put('/:id',categoryController.updateCategory)
router.delete('/:id',categoryController.deleteCategory)
router.get('/:page/:limit',categoryController.getAll)
router.get('/:id',categoryController.getCategoryById)

module.exports=router
 