const subcategoryController = require('../controllers/subcategory.controller')
const express = require('express')
 const router = express.Router()
 
router.post('/',subcategoryController.create)
router.put('/:id',subcategoryController.updateSubCategory)
router.delete('/:id',subcategoryController.deleteSubCategory)
router.get('/:page/:limit',subcategoryController.getAll)
router.get('/:id',subcategoryController.getsubcategoryById)

module.exports=router

