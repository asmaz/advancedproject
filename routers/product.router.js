const productController = require('../controllers/product.controller')
const express = require('express')
 const router = express.Router()

router.post('/',productController.create)
router.put('/:id',productController.updateproduct)
router.delete('/:id',productController.deleteproduct)
router.get('/:page/:limit',productController.getAll)
router.get('/:id',productController.getProductById)

module.exports=router

